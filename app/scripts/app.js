(function(document) {
  'use strict';

  window.CellsPolymer.start({
    routes: {
      'login': '/login',
      'main': '/',
      'order': '/order'
    }
  });
}(document));
